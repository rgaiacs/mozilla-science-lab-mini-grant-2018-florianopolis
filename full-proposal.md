# Full Proposal

Based on https://docs.google.com/document/d/1qPt0NWDIBmZ10F9bsWPGbDeZfy6pl2k177fV4jSoddo/edit#bookmark=id.7hw0titrpfvh.

## Project Purpose

"PythonPolis: Python and Open Science Tools for Scientists" is a curriculum creation sprint
to produce content of Python for scientists.

## Describe the issue/problem you are trying to address

The purpose of our project is to help Brazilian Oceanography, Biology, Geosciences and Meteorology researchers, graduate and undergraduate students on their adoption of Free and Open Source Software (FOSS) in their research and in turn help them enter the world of Open Science, mainly pointing to Open Source Software, but also referring to Reproducible Research, Open and FAIR (findable, accessible, interoperable and reusable) Data.

Melissa Weber Mendonça is a Professor at Federal University of Santa Catarina's Mathematics department since 2010. As a mathematical programming and scientific computing teacher and researcher, she has collaborated with the Oceanography department at her home institution for the last 3 years. In 2016, she co-organized and taught a Python workshop for Oceanographers and Biologists. At the time, the demand for the workshop was very high: there were 30 participant slots available and more than 100 applications. The Oceanography department was interested in organizing it again, this time with a more specialized focus on applications related to Oceanography and Geosciences.

Many scientists in Brazil are not using Python because most universities teach MATLAB as the main tool for analysis and other computational applications. However, there is a general understanding that using a proprietary system is not ideal for Open Science, and there is a great interest in Python as a language. Unfortunately, teaching materials in Portuguese are still scarce, especially for people who are not particularly skilled in programming. Not having materials in Portuguese is one of the barriers to enter the computer field as mentioned by Andy Oram at Open Source in Brazil.

This project aims to address the issues described above by financing a 5 day lesson-curriculum creation sprint. The expected output will be a 12-hours open curriculum covering the basics of Python and related libraries, when relevant providing examples and applications for researchers in the fields mentioned above. The curriculum will be available under the CC-BY 4.0 license allowing all Portuguese-speaking students, researchers and other interested people to benefit from this project.

Finally, by leveraging local Research Software Engineer champions, this project will help promote a culture change around collaborative and open lesson development in the Brazilian research community, and promote the role of software and research software engineers within the research environment.

## List key project activities (what will you do), outputs (what will be produced through your activities, e.g. products, publications, number of workshops held and people trained) and outcomes (impact of your project on your beneficiaries during the grant period).

**Goals**

1. Promote a free and open source community driven project that leverages the use of FOSS
2. Develop scientific computational materials accessible to Brazilian communities that have so far little access to technical resources

**Activities**

1. Coordinate a lesson curriculum creating sprint
2. Release a lesson curriculum covering the basics of Python and related libraries for Oceanography, Biology, Geosciences and Meteorology
3. Identify open data within the domains of our target audience, which could be use for demonstration in the development of the curriculum
4. Run a 12-hour pilot workshop using the curriculum

For the Curriculum Creation Sprint, we will invite and coordinate a group of 10 professors, researchers, research software engineers, software developers and educators to create, during 9-13 April, a curriculum, licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/), for scientific Python and tools relevant to the areas mentioned above. The curriculum will be designed to educate mostly graduate students but will be also suitable for senior researchers and last year undergraduate students. In the preparation of the lessons, we will take into account the research-based principles for smart teaching as described in the book "How Learning Works" by Ambrose et al, used in the Carpentries Trainer instruction.

The working group will be lead by Melissa Weber Mendonça and Juliana Leonel, two full-time professors at the Federal Univesity of Santa Catarina (UFSC). Melissa and Juliana will host the sprint in Florianópolis, Brazil to increase the engagement of their local community. As external specialists, Melissa and Juliana will invite Filipe Fernandes, Tania Allard, and Alejandra Gonzalez-Beltran. 5 places in the sprint will be offered to the local community in a call for participation.

After the sprint, the authors will continue to collaborate on GitLab polishing the curriculum until the planned release at the end of July 2018. Before the release, Raniere Silva, Aleksandra Pawlik and Renato Augusto Corrêa dos Santos will review the curriculum, provide feedback and suggest amendments to the content and delivery structure of the materials based on their previous experiences providing training to researchers and academics.

The pilot workshop will be hosted at UFSC in August 2018. 40 seats will be offered to Oceanography, Biology, Geosciences and Meteorology researchers. These seats will be assigned via an open call, which will be disseminated using appropriate channels (e.g. university mailing list, social media, partners websites). The applications submitted during the open call will be reviewed and the seats will be assigned on a fair and inclusive/diversity basis (members of the team attended the [Diversity & Inclusion in Scientific Computing Unconference](https://pydata.org/nyc2017/diversity-inclusion/disc-unconference-2017/) organised by NumFOCUS where they gained experience in such process). The pilot workshop will be free of charge to learners and at the end, we will host a dinner to collect feedback and facilitate future collaborations.

After the pilot included in this proposal, the chairs of this proposal will continue to improve the curriculum and deliver it in different formats at their home institution. Initially, this would include a workshop offered every semester, and a possibly shorter version to be offered at conferences. There is also a demand for the inclusion of this course or some variation of it as a permanent semester-long course in the Oceanography department, and maybe others. The curriculum will be modified accordingly so that it addresses domain specific demands, and thus making it more meaningful for the attendees. There are also plans to release the same material in other languages, such as English and Spanish, taking advantage of the networks the team creating the material belongs to.

**Outputs**

- Workshop materials openly licensed
- Feedback from the attendees to the pilot workshop
- A set of follow up feedback (6 months after the pilot workshop)
- An open an inclusive community dedicated to supporting new and veteran users of FOSS

**Outcomes**

With this project, we will encourage students and researchers in Oceanography, Biology, Geosciences and Meteorology to use open source tools and follow open science best practices, considering the notions of reproducibility and open and FAIR data.
We also aim to increase the number of researchers who have the confidence to use and contribute to FOSS, make their science reproducible and their data available.

Other outcomes of this project will be to inspire future open source advocates and drive a culture change at Brazilian institutions by leveraging the use of FOSS in teaching and research. This will, in turn, serve as a catalyst to drive and facilitate change within research and create a culture in which best practices and working in the open are incentivised and recognised as means to maximise research outputs and establish collaborations.

## Provide key indicators you plan to use to measure project outcomes and source of data.

Outcomes for this type of project are hard to measure.
David F. Feldon et al. wrote "[Null effects of boot camps and short-format training for PhD students in life sciences](https://dx.doi.org/10.1073%2Fpnas.1705783114)"
saying that the effects of training like the one we are proposing are null.
We disagree with David F. Feldon et al. and Karen R. Word et al. wrote "[When Do Workshops Work?](http://www.datacarpentry.org/blog/reponse-to-null-effects/)",
a blog post response to David F. Feldon et al. paper.
We will collect pre and post-workshop survey from the attendees of the pilot workshop we will run.
The collected data will be compared with [data available in the literature](https://carpentries.github.io/assessment/carpentries/long-term-survey/report.html)
to assess the pilot and create a list of future changes.
If we obtain funds to continue the project after the initial 6 months,
we will collect a survey of the attendees after 6 months and 1 year of the pilot workshop.

Also, we will consider the following metrics to measure the outcomes:

- The number of page views of the online curriculum
- The number of contributors to the material (suggested edits and additions, contributions to the infrastructure and releases)
- The number of workshops delivered using the generated material and the feedback obtained
- The number of open research software and outputs generated by the attendees
- The emergence of open source communities in the area

## Explain who will benefit from the project and how you will engage with them.

The primary group to benefit from the project will be students and researchers in Oceanography, Biology, Geosciences and Meteorology from the Federal University of Santa Catarina.

The secondary group to benefit from the project will be Portuguese-speaking students and researchers on the subjects mentioned above. Because the curriculum developed as part of this project will be open source, we envision that it will be used by self-learners as well as by study groups in universities based in Brazil and other Portuguese-speaking countries. Members of our primary beneficiary group will be invited to participate in the lesson curriculum creating sprint and on the 12-hour pilot workshop using the curriculum.

To engage with our global group of potential beneficiaries, we will have public Telegram and Facebook groups (if convenient) in addition to GitLab where other users can interact in a less formal way.

## Describe the geographic location(s) in which this project will be implemented.

"PythonPolis: Python and Open Science Tools for Scientists" will be implemented at the Federal University of Santa Catarina in Florianópolis, Brazil.

Florianópolis is the capital of the state of Santa Catarina, in the South region of Brazil. It is composed of one main island, Santa Catarina
Island, a continental part and surrounding small islands. It has a population of around 477k, the 47th most populous city in Brazil.
The economy of Florianópolis is heavily based on information technology, tourism and services.

The Federal University of Santa Catarina (UFSC) was funded in 1960 and in 2016 it had around 45k students in their undergraduate and graduate programmes. The [Times Higher Education Latin America University Rankings 2017](https://www.timeshighereducation.com/world-university-rankings/latin-america-university-rankings-2017-results-out-now) lists UFSC as number 15 in Latin America. The university has strong graduate programs and research projects, and has strong connections to other research institutions in Brazil and abroad, thanks to well established international exchange and international cooperation programs. A considerable percentage of the information technology workforce in Florianópolis was trained at the Federal University of Santa Catarina which will potentialize the impact of our project in the long term.

## List any risks or challenges that may affect the overall success of your project and note how Mozilla and/or others can help you to overcome these challenges.

We don’t foresee risks associated with our project. We hope that the funding provided to this project will help to established a better course that stimulates young researchers to follow open science and make an example than senior researchers will follow.

## List your project partners if any, and the role they will play in the project.

The sprint will be hosted by the Federal University of Santa Catarina, and the pilot workshop will be offered as a (free) extension project there also.

## Mozilla works in the open. How will you document and share your project progress with the community?

The whole process of discussing and writing this proposal is being done in the open on GitLab, https://gitlab.com/rgaiacs/mozilla-science-lab-mini-grant-2018-florianopolis/.
We will continue to use GitLab as the central point of collaboration between the ones involved in this proposal, including for the development of the curriculum that we proposed, which will be available under the Creative Commons Attribution 4.0 license.
To share the project we will create a website, a public Telegram and Facebook groups (if convenient) and some print material available for download upon request. The graphic material created for the project will be available in GitLab under an appropriate open license.

## Are other organizations working toward outcomes similar to those described in this proposal? If yes, explain how your work complements that of others or fills a key gap.

[Software Carpentry](http://software-carpentry.org/) and [Data Carpentry](http://www.datacarpentry.org/)
develop and teach domain specific, from life and physical sciences to social science, lessons
on the fundamental software development and data skills needed to conduct research.
Their initial target audience is learners who have little to no prior computational experience,
same as ours.
Software Carpentry and Data Carpentry don't have a lesson on Oceanography, Geosciences and Meteorology
and our proposal will fill this gap with the possibility of being reused by them.
Raniere Silva is serving on their Executive Council which will ensure a close relationship.

Renato Augusto Corrêa dos Santos has been organising activities in Brazil to teach biologists
to code in his local community. Renato will be one of the reviewers of the curriculum
and might use it in his future workshops.

Tania Allard and Alejandra Gonzalez-Beltran are fellows of the [Software Sustainability Institute](https://www.software.ac.uk/)
and are engaged in promoting best practices around software and data and reproducible research from postgraduate and early career stages
in their local communities in the UK.

## Describe how this project fits into your organization’s mission and goals.

**Skip.**

## How does this project contribute to a healthier Internet?

By promoting Open Science and Open Source, we will make students and academics aware of the issues around Free and Open Source Software, including Open Data, Net Neutrality and other related factors.

## Is this a new project or a continuation? If new, please describe your qualifications to initiate the activity. If continued, please describe your accomplishments to date. Feel free to include links to articles and documents online that highlight your recent work.

This is a new project that will build on members' previous experience, but it will be extended to a much larger audience and contribute to avoiding duplicate effort in the future. Melissa Weber Mendonça leads a [Python curriculum for Oceanography](https://github.com/melissawm/oceanobiopython) and workshop based on it in 2016. Filipe Fernandes taught a [workshop during the 7th Brazilian Oceanography Congress](https://github.com/ocefpaf/2016-Python-course-CBO) in 2016. Renato A. Correa dos Santos taught a work for biology PhD candidates at the University of São Paulo in 2017.

## How will you continue work on this project beyond this funding period?

Melissa Weber Mendonça and Juliana Leonel are in a permanent contract with the Federal University of Santa Catarina
and funds to continue the use of curriculum as part of their teaching working time is secure.
Renato Augusto Corrêa dos Santos and Raniere Silva can facilitate the contact with possible hosts for future workshops that use the curriculum in Brazil.

With the sprint, Melissa hopes to build momentum to pitch local funds to create a [Research Software Engineers](http://rse.ac.uk/) community that will contribute to the development and delivery of the curriculum.

## Have you previously partnered with Mozilla? Have you previously received a grant from Mozilla? If so, please list title, amount, and purpose.

This is the first time that Melissa Weber Mendonça is partnering with Mozilla. Other members of the project team have partnered with Mozilla in the past as described below:

- Tania Allard is currently involved with Mozilla Open Leaders.
- Raniere Silva was involved with Mozilla as part of his Google Summer of Code in 2014 working on Firefox OS.

## Project Team

+--------------------+--------------------+-------------------------------+----------------------------+
|Name                |Institution         |E-Mail                         |Contribution to and         |
|                    |                    |                               |role                        |
+====================+====================+===============================+============================+
|Melissa Weber       |Federal University  |melissawm@gmail.com            |Co-Chair, local organiser,  |
|Mendonça            |of Santa Catarina   |                               |lesson creator, lead        |
|                    |                    |                               |instructor.                 |
|                    |                    |                               |                            |
+--------------------+--------------------+-------------------------------+----------------------------+
|Juliana Leonel      |Federal University  |juoceano@gmail.com             |Co-Chair, local organiser,  |
|                    |of Santa Catarina   |                               |lesson creator.             |
|                    |                    |                               |                            |
|                    |                    |                               |                            |
+--------------------+--------------------+-------------------------------+----------------------------+
|Filipe Fernandes    |Southeast Coastal   |ocefpaf@gmail.com              |Python Software Engineer    |
|                    |Ocean Observing     |                               |consultant with experience  |
|                    |Regional Association|                               |in Oceanography.            |
|                    |                    |                               |                            |
|                    |                    |                               |                            |
+--------------------+--------------------+-------------------------------+----------------------------+
|Tania Allard        |University of       | t.allard@sheffield.ac.uk      |Researcher Software Engineer|
|                    |Sheffield           |                               |consultant in Python        |
|                    |                    |                               |training.                   |
|                    |                    |                               |                            |
+--------------------+--------------------+-------------------------------+----------------------------+
|Alejandra           | University of      | alejandra.gonzalezbeltran@    |Researcher Software Engineer|
|Gonzalez-Beltran    | Oxford             | oerc.ox.ac.uk                 |consutant in data           |
|                    |                    |                               |management.                 |
|                    |                    |                               |                            |
+--------------------+--------------------+-------------------------------+----------------------------+
|Raniere Silva       |University of       |raniere@rgaiacs.com            |Lesson creator advisor.     |
|                    |Manchester          |                               |                            |
|                    |                    |                               |                            |
|                    |                    |                               |                            |
+--------------------+--------------------+-------------------------------+----------------------------+
|Aleksandra Pawlik   |New Zealand eScience|aleksandra.pawlik@gmail.com    |Lesson creator advisor.     |
|                    |Infrastructure      |                               |                            |
|                    |                    |                               |                            |
+--------------------+--------------------+-------------------------------+----------------------------+
|Renato A. Correa dos|Campinas State      |renatoacsantos@gmail.com       |Lesson creator advisor.     |
|Santos              | University         |                               |                            |
|                    |                    |                               |                            |
|                    |                    |                               |                            |
+--------------------+--------------------+-------------------------------+----------------------------+

Melissa Weber Mendonça is a Professor at Federal University of Santa Catarina's Mathematics department since 2010. She is an applied mathematician, with a passion for Free Software and Open Science. As a mathematical programming and scientific computing teacher and researcher, she has collaborated with the Oceanography department at her home institution for the last 3 years. In 2016, she co-organized and taught a Python workshop for Oceanographers and Biologists.

Juliana Leonel is a Professor at the Oceanography department of the Federal University of Santa Catarina. 

Filipe Fernandes is an oceanographer by training, software developer by opportunity, educator for passion. Filipe works for U.S. Integrated Ocean Observing System (IOOS), a task force of NOAA 
that joins people and technology to better develop oceanography observation and modelling tools, where Python, R, and other data server technologies are key to sucess. In his free time, 
Filipe has fun contributing to conda-forge and teaching better software development practices as a Software Carpentry instructor.

Tania Allard is a Research Software Engineer with experience developing lesson content for the OpenDreamKit project as well a for OOOMinds, a Leverhulme fellowship project.
Tania has significant experience working on multi-disciplinary projects as well as in data and software engineering.
As a research software engineer she is particularly involved in the development of complex data analysis workflows as well as adequate data curation and preservation strategies.
She is particularly interested in the reproducible and replicable scientific research and thus has working experience in developing standards and tools for researchers and data scientists
to ensure best practices in their workflows while maximising their research impact.
In addition, she has over 8 years teaching experience at different levels of education and has taken courses in pedagogy and teaching methodologies.

[Alejandra Gonzalez-Beltran](https://orcid.org/0000-0003-3499-8262) is a Research Lecturer at the [Oxford e-Research Centre](http://oerc.ox.ac.uk/), [Department of Engineering Science](http://www.eng.ox.ac.uk/) at the [University of Oxford](http://www.ox.ac.uk/).
Alejandra has experience data science and software engineering, including expertise on data standards and knowledge management working on multi-disciplinary
domains, including biomedical sciences and computer science. She is involved in infrastucture projects supporting and enabling open science and open data such as the [ISA-tools framework for data management](http://isa-tools.org), including an open source Python-based [ISA-API](github.com/iSA-tools/isa-api) for experimental metadata management focused for the life sciences, and the [FAIRsharing](http://fairsharing.org) registry of standards, databases and policies. 
She also has vast experience in teaching in different levels of education, is a Carpentry Instructor and will complete the Instructor Trainer
training in early 2018. In addition, she has been selected as a [UK Software Sustainability](https://www.software.ac.uk/) Fellow for 2018. Alejandra's background is in Computer Science, having received a Licentiateship in Computer Science (would be equivalent to an MSc)
from [National University of Rosario](http://unr.edu.ar/), Argentina and a PhD in Computer Science from [Queen's University Belfast](https://www.qub.ac.uk/), UK.

## Budget

See [budget.ods](budget.ods).
